/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeetraining;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author tbarrett
 */
public class ReportViewer extends javax.swing.JFrame {

    private Connection con = null;//globals
    private ResultSet rs = null;
    private Statement stmt = null;
    private String Employee = null;
    private String Supervisor = null;
    
    
    /**
     * Creates new form ReportViewer
     */
    public ReportViewer() {
        initComponents();
        String onLoadQuery = "Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME";
        try {
            ERPConnect conn = new ERPConnect();
            con = conn.ERPConnect();
            stmt = con.createStatement();
            rs = stmt.executeQuery(onLoadQuery);
            
            while (rs.next()){
                employeeText.addItem(rs.getString("EMP_NUM") + "  |  " + rs.getString("NAME"));
            }
            
            con.close();
            stmt.close();
            rs.close();
        }
        catch (Exception x){
            x.printStackTrace();
        }
        
        employeeText.addActionListener(new ActionListener (){ //action listener for combobox selected item change
            public void actionPerformed(ActionEvent e){
                Employee = employeeText.getSelectedItem().toString();
                String trainingHDRQuery = "SELECT EMP_NUM, EMP_NAME, Job_Title, Create_Date, Question, JTScore, Created_By, "
                        + "Revisit, FullyTrainedOnRequiredWI FROM tblTraining_HDR WHERE EMP_NUM = '" + Employee.substring(0, 4) + "'";
                String EXTNEWEMPQuery = "SELECT EMP_NUM, EMP_NAME, Date_Created, Created_By, tailstretcherQ1, "
                        + "sawhelperQ1, presshelperQ1, pressteamQ1, pressteamQ2, pressteamQ3, pressteamQ4, pressteamQ5, pressteamQ6, PressTeamJobTitle "
                        + "FROM tblEXT_NEW_EMP WHERE EMP_NUM = '" + Employee.substring(0, 4) + "'";
                String FABNEWEMPQuery = "SELECT EMP_NUM, EMP_NAME, Date_Created, Created_By, metalsawsQ1, custommachinesQ1, "
                        + "towQ1, craneQ1, fabpressQ1, combieQ1 FROM dbo.tblFAB_NEW_EMP WHERE EMP_NUM = '" + Employee.substring(0, 4) + "'";
                String SHIPNEWEMPQuery = "SELECT EMP_NUM, EMP_NAME, Date_Created, Created_By, shipQ1 FROM dbo.tblSHIP_NEW_EMP "
                        + "WHERE EMP_NUM = '" + Employee.substring(0, 4) + "'";
               try {
                    SQLConnect conn = new SQLConnect();
                    con = conn.SQLConnect();
                    stmt = con.createStatement();
                    rs = stmt.executeQuery(trainingHDRQuery);

                    trainingHDRTable.setModel(buildTableModel(rs));
                    rs.close(); //close first resultset
                    
                    rs = stmt.executeQuery(EXTNEWEMPQuery);
                    extRecordTable.setModel(buildTableModel(rs));
                    rs.close();
                    
                    rs = stmt.executeQuery(FABNEWEMPQuery);
                    fabRecordTable.setModel(buildTableModel(rs));
                    rs.close();
                    
                    rs = stmt.executeQuery(SHIPNEWEMPQuery);
                    shipRecordTable.setModel(buildTableModel(rs));
                    rs.close();                                     
                    

                    con.close();
                    stmt.close();
                    rs.close();

               }
               catch (Exception ex){
                   ex.printStackTrace();
               }
            }
        });
        
    }

    public static DefaultTableModel buildTableModel(ResultSet rs)
        throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
            
        }
      //  columnNames.add("JTScore");
        
        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            } 
      
            data.add(vector);
            
            
            
        }

        return new DefaultTableModel(data, columnNames);

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        employeeText = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        trainingHDRTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        shipRecordTable = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        extRecordTable = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        fabRecordTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Report Viewer");

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jLabel1.setText("Employee Record:");

        trainingHDRTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(trainingHDRTable);

        shipRecordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(shipRecordTable);

        extRecordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(extRecordTable);

        fabRecordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(fabRecordTable);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(employeeText, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1393, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 1393, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 1393, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(employeeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 342, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(195, 195, 195)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(390, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(361, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(224, 224, 224)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReportViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReportViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReportViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReportViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ReportViewer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> employeeText;
    private javax.swing.JTable extRecordTable;
    private javax.swing.JTable fabRecordTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable shipRecordTable;
    private javax.swing.JTable trainingHDRTable;
    // End of variables declaration//GEN-END:variables
}
