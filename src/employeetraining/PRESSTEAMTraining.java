/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeetraining;

import static employeetraining.TrainingHDR.buildTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author tbarrett
 */
public class PRESSTEAMTraining extends javax.swing.JFrame {

    private Connection con = null;//globals
    private ResultSet rs = null;
    private Statement stmt = null;
    private String Employee = null;
    private String Supervisor = null;
    private String jobTitle = null;
    JList list;
            
    String headStretcher[] = {
            "1.Capable of operating computer to view work instructions, prints, etc.",
            "2.Capable of using a tape measure & calipers? ",
            "3.Capable of updating SPC charts (when necessary)?",
            "4.Capable of working independently?",
            "5.Can adequately operate Stretcher equipment?",
            "6.Capable of head stretching?"
            };
    String billetMan[] = {
            "1.Capable of operating computer to view work instructions, prints, etc.",
            "2.Capable of using a tape measure & calipers?",
            "3.Capable of updating SPC charts (when necessary)?",
            "4.Capable of working independently?",
            "5.Can adequately operate the Billet Magazine?",
            "6.Has received proper training for Combie Lift?"
            };
    String dieHeader[] = {
            "1.Capable of operating computer to view work instructions, prints, etc.",
            "2.Capable of using a tape measure & calipers?",
            "3.Capable of updating SPC charts (when necessary)?",
            "4.Capable of working independently?",
            "5.Can adequately operate Die Header equipment?",
            "6.Has received proper training for Overhead Crane?"
            };
    String sawMan[] = {
            "1.Capable of operating computer to view work instructions, prints, etc.",
            "2.Capable of using a tape measure & calipers?",
            "3.Capable of updating SPC charts (when necessary)?",
            "4.Capable of working independently?",
            "5.Can adequately operate the Saw?"
            };
    String pressOperator[] = {
            "1.Capable of operating computer to view work instructions, prints, etc.",
            "2.Capable of operating necessary machines and/or equipment?",
            "3.Capable of using a tape measure & calipers?",
            "4.Capable of updating SPC charts (when necessary)?",
            "5.Capable of working independently?",
            "6.Can adequately operate the Press?"
            };
    String[] columnNames = {"Question", "Confirm"};
    
    /**
     * Creates new form PRESSTEAMTraining
     */
    public PRESSTEAMTraining(String employee, String supervisor) {
        initComponents();
        Employee = employee;
        Supervisor = supervisor;
        employeeText.setText(Employee);
        supervisorText.setText(supervisor);
        
        
        
        pressTeamComboBox.addActionListener(new ActionListener (){ //action listener for combobox selected item change
            public void actionPerformed(ActionEvent e){
                //do something
                pressCheckBox6.setVisible(false);
                jobTitle = pressTeamComboBox.getSelectedItem().toString();
              try{
                  
              
                switch (jobTitle){
                    case "Head Stretcher":                      
                        questionList.setListData(headStretcher);   
                        pressCheckBox6.setVisible(true);
                        break;
                    case "Billet Man":
                        questionList.setListData(billetMan);
                        pressCheckBox6.setVisible(true);
                        break;
                    case "Die Header":
                        questionList.setListData(dieHeader);
                        pressCheckBox6.setVisible(true);
                        break;
                    case "Saw Man":
                        questionList.setListData(sawMan);
                        pressCheckBox6.setVisible(false);
                        break;
                    case "Press Operator":
                        questionList.setListData(pressOperator);
                        pressCheckBox6.setVisible(true);
                        break;
                                            
                }
              }
              catch (Exception x){
                  JOptionPane.showMessageDialog(null, x);
              }
            }
        });
        
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        employeeText = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        supervisorText = new javax.swing.JTextField();
        submitButton = new javax.swing.JButton();
        pressTeamComboBox = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        questionList = new javax.swing.JList<>();
        pressCheckBox1 = new javax.swing.JCheckBox();
        pressCheckBox2 = new javax.swing.JCheckBox();
        pressCheckBox3 = new javax.swing.JCheckBox();
        pressCheckBox4 = new javax.swing.JCheckBox();
        pressCheckBox5 = new javax.swing.JCheckBox();
        pressCheckBox6 = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Press Team");

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jLabel1.setText("Press Team");

        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        pressTeamComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Head Stretcher", "Billet Man", "Die Header", "Saw Man", "Press Operator" }));

        jScrollPane2.setViewportView(questionList);

        pressCheckBox1.setText("1. Confirm");

        pressCheckBox2.setText("2. Confirm");

        pressCheckBox3.setText("3. Confirm");

        pressCheckBox4.setText("4. Confirm");

        pressCheckBox5.setText("5. Confirm");

        pressCheckBox6.setText("6. Confirm");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(pressTeamComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(submitButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(employeeText, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(supervisorText))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pressCheckBox2)
                            .addComponent(pressCheckBox1)
                            .addComponent(pressCheckBox3)
                            .addComponent(pressCheckBox4)
                            .addComponent(pressCheckBox5)
                            .addComponent(pressCheckBox6))
                        .addGap(20, 20, 20))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(pressTeamComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(pressCheckBox1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pressCheckBox2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pressCheckBox3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pressCheckBox4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pressCheckBox5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pressCheckBox6)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(employeeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(supervisorText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(submitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(106, 106, 106))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        // TODO add your handling code here:
        // Press Team insert record
        boolean pressQuestion1 = pressCheckBox1.isSelected();
        boolean pressQuestion2 = pressCheckBox2.isSelected();
        boolean pressQuestion3 = pressCheckBox3.isSelected();
        boolean pressQuestion4 = pressCheckBox4.isSelected();
        boolean pressQuestion5 = pressCheckBox5.isSelected();
        boolean pressQuestion6 = pressCheckBox6.isSelected();
        jobTitle = pressTeamComboBox.getSelectedItem().toString();
        Employee = employeeText.getText();
        Supervisor = supervisorText.getText();
        String now = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        String insertQuery = "Insert into tblEXT_NEW_EMP (EMP_NUM, EMP_NAME, Date_Created, Created_By, pressteamQ1, pressteamQ2, pressteamQ3, "
                + "pressteamQ4, pressteamQ5, pressteamQ6, PressTeamJobTitle) SELECT '" + Employee.substring(0, 4) + "' as EMP_NUM_Ins, '" + Employee.substring(9) + "' as EMP_NAME_Ins, '"
                + now + "' as Date_Created_INS, '" + Supervisor + "' as Created_By_INS, '" + pressQuestion1 + "' as pressteamQ1_INS, '"
                + pressQuestion2 + "' as pressteamQ2_INS, '" + pressQuestion3 + "' as pressteamQ3_INS, '" + pressQuestion4 + "' as pressteamQ4_INS, '"
                + pressQuestion5 + "' as pressteamQ5_INS, '" + pressQuestion6 + "' as pressteamQ6_INS, '" + jobTitle + "' as PressTeamJobTitle_INS";
                
        
        try{
            SQLConnect conn = new SQLConnect();
            con = conn.SQLConnect();
            stmt = con.createStatement();
            stmt.execute(insertQuery);
            
            JOptionPane.showMessageDialog(null, "You have successfully updated this record.");
            con.close();
            stmt.close();
            this.dispose();
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
                                


    }//GEN-LAST:event_submitButtonActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PRESSTEAMTraining.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PRESSTEAMTraining.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PRESSTEAMTraining.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PRESSTEAMTraining.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new PRESSTEAMTraining().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField employeeText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JCheckBox pressCheckBox1;
    private javax.swing.JCheckBox pressCheckBox2;
    private javax.swing.JCheckBox pressCheckBox3;
    private javax.swing.JCheckBox pressCheckBox4;
    private javax.swing.JCheckBox pressCheckBox5;
    private javax.swing.JCheckBox pressCheckBox6;
    private javax.swing.JComboBox<String> pressTeamComboBox;
    private javax.swing.JList<String> questionList;
    private javax.swing.JButton submitButton;
    private javax.swing.JTextField supervisorText;
    // End of variables declaration//GEN-END:variables
}
