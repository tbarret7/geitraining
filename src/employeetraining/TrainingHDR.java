/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeetraining;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.engine.JRDataSource;

/**
 *
 * @author tbarrett
 */
public class TrainingHDR extends javax.swing.JFrame {

    /**
     * Creates new form TrainingHDR
     */
    private Connection con = null;//globals
    private ResultSet rs = null;
    private Statement stmt = null;
    private String jobTitle = null;
    private String jtScore = null;
    private String jobQuestions = null;
    private String[] scores = {"Needs Training", "Adequate", "Proficient"};
    private JComboBox JTScore = new JComboBox<String>(scores);
    
    public TrainingHDR() { //on-load
        initComponents();
                
        String onloadQuery = "Select Job_Title from tblJob_Title_Ques_XREF GROUP BY Job_Title";
        String onLoadQuery = "Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME";
        SQLConnect conn = new SQLConnect();
        ERPConnect connn = new ERPConnect();
        try {
            con = conn.SQLConnect();
            stmt = con.createStatement();
            rs = stmt.executeQuery(onloadQuery);
            
            while (rs.next()){
                jobTitleText.addItem(rs.getString("Job_Title"));
            }
            con.close();
            stmt.close();
            rs.close();
        }
        catch (SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex);
        }
        try {
            con = connn.ERPConnect();
            stmt = con.createStatement();
            rs = stmt.executeQuery(onLoadQuery);
            
            while (rs.next()){
                employeeText.addItem(rs.getString("EMP_NUM") + "  |  " + rs.getString("NAME"));
            }
            
            con.close();
            stmt.close();
            rs.close();
        }
        catch (Exception x){
            x.printStackTrace();
        }
        
        jobTitleText.addActionListener(new ActionListener (){ //action listener for combobox selected item change
            public void actionPerformed(ActionEvent e){
                jobTitle = jobTitleText.getSelectedItem().toString();
                String questionQuery = "SELECT Question FROM dbo.tblJob_Title_Ques_XREF WHERE Job_Title = '" + jobTitle + "'";
                
               try {
                    con = conn.SQLConnect();
                    stmt = con.createStatement();
                    rs = stmt.executeQuery(questionQuery);
                    
                    DefaultTableModel model = buildTableModel(rs);
                    
                    trainingHDRTable.setModel(model);
                    TableColumn col = trainingHDRTable.getColumnModel().getColumn(1);                   
                    col.setCellEditor(new DefaultCellEditor(JTScore));      
                    TableColumnModel tcm = trainingHDRTable.getColumnModel();
                    tcm.getColumn(0).setPreferredWidth(300);
                    tcm.getColumn(1).setPreferredWidth(40);

                    con.close();
                    stmt.close();
                    rs.close();

               }
               catch (Exception ex){
                   ex.printStackTrace();
               }
            }
        });
        
    }
public static DefaultTableModel buildTableModel(ResultSet rs)
        throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
            
        }
        columnNames.add("JTScore");
        
        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            } 
      
            data.add(vector);
            
        }

        return new DefaultTableModel(data, columnNames);

}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        employeeText = new javax.swing.JComboBox<>();
        jobTitleText = new javax.swing.JComboBox<>();
        revisitText = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        trainingHDRTable = new javax.swing.JTable();
        submitTrainingButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        requiredWIText = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        extTraining = new javax.swing.JButton();
        shipTraining = new javax.swing.JButton();
        fabPress = new javax.swing.JButton();
        fabTraining = new javax.swing.JButton();
        pressTeam = new javax.swing.JButton();
        vehicleButton = new javax.swing.JButton();
        reportViewerButton = new javax.swing.JButton();
        printCoachingButton = new javax.swing.JButton();
        reportButton = new javax.swing.JButton();
        scoresButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Employee Training");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        trainingHDRTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(trainingHDRTable);

        submitTrainingButton.setText("Submit");
        submitTrainingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitTrainingButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Employee Name:");

        jLabel3.setText("Job Title:");

        jLabel4.setText("Revisit Required:");

        requiredWIText.setText("Fully trained on WI Required");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(submitTrainingButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(employeeText, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel2)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jobTitleText, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(52, 52, 52)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel4))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(revisitText, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(requiredWIText)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 624, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(employeeText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jobTitleText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(revisitText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(requiredWIText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(submitTrainingButton, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel1.setText("Main Menu:");

        extTraining.setText("EXT Training");
        extTraining.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                extTrainingActionPerformed(evt);
            }
        });

        shipTraining.setText("SHIP Training");
        shipTraining.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shipTrainingActionPerformed(evt);
            }
        });

        fabPress.setText("Fab Presses");
        fabPress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fabPressActionPerformed(evt);
            }
        });

        fabTraining.setText("FAB Training");
        fabTraining.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fabTrainingActionPerformed(evt);
            }
        });

        pressTeam.setText("Press Team");
        pressTeam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pressTeamActionPerformed(evt);
            }
        });

        vehicleButton.setText("Vehicles");
        vehicleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vehicleButtonActionPerformed(evt);
            }
        });

        reportViewerButton.setText("Data Viewer");
        reportViewerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportViewerButtonActionPerformed(evt);
            }
        });

        printCoachingButton.setText("Print Coaching");
        printCoachingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printCoachingButtonActionPerformed(evt);
            }
        });

        reportButton.setText("Reporting");
        reportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportButtonActionPerformed(evt);
            }
        });

        scoresButton.setText("Avg. Scores");
        scoresButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scoresButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(reportButton, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(scoresButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(reportViewerButton, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(printCoachingButton, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(fabPress, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(vehicleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(shipTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pressTeam, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(extTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fabTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(extTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fabTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(shipTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pressTeam, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fabPress, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(vehicleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reportViewerButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(printCoachingButton, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(scoresButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(reportButton, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void submitTrainingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitTrainingButtonActionPerformed
        // TODO add your handling code here:
        //Insert trainingHDRTable data to create new record
        
        String varNum = null;
        String varName = null;
        jobTitle = jobTitleText.getSelectedItem().toString();
        String revisit = revisitText.getText();
        String now = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE);        
        varNum = employeeText.getSelectedItem().toString();
        varName = employeeText.getSelectedItem().toString();
        
        SQLConnect conn = new SQLConnect();
       
        try{
            
            TableModel model = trainingHDRTable.getModel();
            String insertQuery = "Insert into tblTraining_HDR (EMP_NUM, EMP_NAME, Job_Title, Question, "
                    + "JTScore, Created_By, Revisit, Create_Date, FullyTrainedOnRequiredWI) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
            for (int row = 0; row < model.getRowCount(); row++)
            {
                jtScore = model.getValueAt(row, 1).toString();
                jobQuestions = model.getValueAt(row, 0).toString();                    
                
                con = conn.SQLConnect();
                PreparedStatement stmnt = con.prepareStatement(insertQuery);
                stmnt.setString(1, varNum.substring(0, 4));
                stmnt.setString(2, varName.substring(9));
                stmnt.setString(3, jobTitle);
                stmnt.setString(4, jobQuestions);
                stmnt.setString(5, jtScore);
                stmnt.setString(6, System.getenv("USERNAME").toString());
                stmnt.setString(7, revisit);
                stmnt.setString(8, now);
                stmnt.setBoolean(9, requiredWIText.isSelected());
                stmnt.execute();
            }
            JOptionPane.showMessageDialog(null, "You have successfully updated this employee record.");
            con.close();
            
        }
        catch (Exception ex){
            JOptionPane.showMessageDialog(null, "Error inputting this record. Please check all related fields for errors, then resubmit.");
       }
    }//GEN-LAST:event_submitTrainingButtonActionPerformed

    private void extTrainingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_extTrainingActionPerformed
        // TODO add your handling code here:
        // EXT Training
        String employee = employeeText.getSelectedItem().toString();
        String supervisor = System.getenv("USERNAME").toString();        
        new EXTTraining(employee, supervisor).setVisible(true);
        
    }//GEN-LAST:event_extTrainingActionPerformed

    private void fabTrainingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fabTrainingActionPerformed
        // TODO add your handling code here:
        // FAB Training
        String employee = employeeText.getSelectedItem().toString();
        String supervisor = System.getenv("USERNAME").toString();
        new FABTraining(employee, supervisor).setVisible(true);
        
    }//GEN-LAST:event_fabTrainingActionPerformed

    private void shipTrainingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shipTrainingActionPerformed
        // TODO add your handling code here:
        // SHIP Training
        String employee = employeeText.getSelectedItem().toString();
        String supervisor = System.getenv("USERNAME").toString();
        new SHIPTraining(employee, supervisor).setVisible(true);
    }//GEN-LAST:event_shipTrainingActionPerformed

    private void pressTeamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pressTeamActionPerformed
        // TODO add your handling code here:
        // PRESS TEAM Training
        String employee = employeeText.getSelectedItem().toString();
        String supervisor = System.getenv("USERNAME").toString();
        new PRESSTEAMTraining(employee, supervisor).setVisible(true);
    }//GEN-LAST:event_pressTeamActionPerformed

    private void fabPressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fabPressActionPerformed
        // TODO add your handling code here:
        // FAB PRESS Training
        String employee = employeeText.getSelectedItem().toString();
        String supervisor = System.getenv("USERNAME").toString();
        new FABPRESSTraining(employee, supervisor).setVisible(true);
    }//GEN-LAST:event_fabPressActionPerformed

    private void vehicleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vehicleButtonActionPerformed
        // TODO add your handling code here:
        // VEHICLES 
        String employee = employeeText.getSelectedItem().toString();
        String supervisor = System.getenv("USERNAME").toString();
        new VEHICLETraining(employee, supervisor).setVisible(true);
    }//GEN-LAST:event_vehicleButtonActionPerformed

    private void reportViewerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportViewerButtonActionPerformed
        // TODO add your handling code here:
        //Report viewer
        new ReportViewer().setVisible(true);
    }//GEN-LAST:event_reportViewerButtonActionPerformed

    private void printCoachingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printCoachingButtonActionPerformed
        // TODO add your handling code here:
        // Print Coaching forms
        String pdfpath = "Y:\\Production\\Training\\Employee_Training\\Coaching_Sheet.xlsm";
         try{
             Desktop desktop = null;
         
            if (Desktop.isDesktopSupported()) {
                desktop = Desktop.getDesktop();
            }
            desktop.print(new File(pdfpath));
         }
         catch (Exception e){
             JOptionPane.showMessageDialog(null, e);
         }
    }//GEN-LAST:event_printCoachingButtonActionPerformed

    private void reportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportButtonActionPerformed
        // TODO add your handling code here:
        SQLConnect conn = new SQLConnect(); 
        String imgPath = "C:\\Program Files\\EmployeeTraining\\lib\\";
        String report = "DeficiencyReport.jasper";
        InputStream reportFile = null;
        
        String employeeNameParam = employeeText.getSelectedItem().toString();
        String jobTitleParam = jobTitleText.getSelectedItem().toString();
        String dateParam = null;
        String question1Param = null;
        String question2Param = null;
        String question3Param = null;
        String question4Param = null;       
        String score1Param = null;
        String score2Param = null;
        String score3Param = null;
        String score4Param = null;
        
        ArrayList<String> questions = new ArrayList<String>();
        ArrayList<String> scores = new ArrayList<String>();

        String reportQuery = "SELECT EMP_NUM, EMP_NAME, Job_Title, Create_Date, Question, JTScore FROM tblTraining_HDR"
                + " WHERE EMP_NUM = '" + employeeNameParam.substring(0, 4) + "' AND Job_Title = '" + jobTitleParam + "'";
        
        
        try{
            con = conn.SQLConnect();
            stmt = con.createStatement();
            rs = stmt.executeQuery(reportQuery);
            
            while(rs.next()){
                dateParam = rs.getString("Create_Date");
                questions.add(rs.getString("Question"));
                scores.add(rs.getString("JTScore"));
            }
            
            
            question1Param = questions.get(0);
            question2Param = questions.get(1);
            question3Param = questions.get(2);
            question4Param = questions.get(3);
            score1Param = scores.get(0);
            score2Param = scores.get(1);
            score3Param = scores.get(2);
            score4Param = scores.get(3);

            Map hm = new HashMap();
            
            hm.put("employeeName", employeeNameParam.substring(9));
            hm.put("employeeBadgeNumber", employeeNameParam.substring(0, 4));
            hm.put("jobTitle", jobTitleParam);
            hm.put("dateCreated", dateParam);
            hm.put("question1", question1Param);            
            hm.put("question2", question2Param);
            hm.put("question3", question3Param);
            hm.put("question4", question4Param);            
            hm.put("score1", score1Param);
            hm.put("score2", score2Param);
            hm.put("score3", score3Param);
            hm.put("score4", score4Param);
            hm.put("imgPath", imgPath);
                    
           
            
            reportFile = getClass().getResourceAsStream(report);
            JasperPrint jp = JasperFillManager.fillReport(reportFile, hm, new JREmptyDataSource());

          //  JasperReport jr = JasperCompileManager.compileReport("src/employeetraining/DeficiencyReport.jrxml");
          //  JasperPrint jp = JasperFillManager.fillReport(jr, hm, new JREmptyDataSource());
            JasperViewer.viewReport(jp, false); // 2nd argument is CLOSEPARENTONEXIT
            con.close();
            stmt.close();
            rs.close();
            
        }
        catch (Exception x){
            JOptionPane.showMessageDialog(null, x);
         //   JOptionPane.showMessageDialog(null, "There are no records for: \nName:" + employeeNameParam.substring(9) + "\nJob Title: " + jobTitleParam);
         // x.printStackTrace();
        }
    }//GEN-LAST:event_reportButtonActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        this.setIconImage(new ImageIcon(getClass().getResource("network.png")).getImage());
        
    }//GEN-LAST:event_formWindowOpened

    private void scoresButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scoresButtonActionPerformed
        // TODO add your handling code here:
        // Open scoring viewer
        new ScoringViewer().setVisible(true);
    }//GEN-LAST:event_scoresButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TrainingHDR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TrainingHDR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TrainingHDR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TrainingHDR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TrainingHDR().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> employeeText;
    private javax.swing.JButton extTraining;
    private javax.swing.JButton fabPress;
    private javax.swing.JButton fabTraining;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jobTitleText;
    private javax.swing.JButton pressTeam;
    private javax.swing.JButton printCoachingButton;
    private javax.swing.JButton reportButton;
    private javax.swing.JButton reportViewerButton;
    private javax.swing.JCheckBox requiredWIText;
    private javax.swing.JTextField revisitText;
    private javax.swing.JButton scoresButton;
    private javax.swing.JButton shipTraining;
    private javax.swing.JButton submitTrainingButton;
    private javax.swing.JTable trainingHDRTable;
    private javax.swing.JButton vehicleButton;
    // End of variables declaration//GEN-END:variables
}
