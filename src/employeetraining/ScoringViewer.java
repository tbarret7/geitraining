/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeetraining;



import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JOptionPane;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
/**
 *
 * @author tbarrett
 */
public class ScoringViewer extends javax.swing.JFrame {

    private Connection con = null;//globals
    private ResultSet rs = null;
    private Statement stmt = null;
   
    /**
     * Creates new form ScoringViewer
     */
    public ScoringViewer() {
        initComponents();
        String availableYears = "SELECT Create_Date FROM tblTraining_HDR";
        String years = null;        
        ArrayList<Integer> results = new ArrayList();
        SQLConnect conn = new SQLConnect();
        String formatted = null;
        try{
            con = conn.SQLConnect();
            stmt = con.createStatement();
            rs = stmt.executeQuery(availableYears);
            
            while (rs.next()){
              
                formatted = rs.getDate("Create_Date").toString();
                formatted = formatted.substring(0,4);
                
                results.add(Integer.valueOf(formatted));
                
            }
            
            List<Integer> listWithoutDuplicates = results.stream().distinct().collect(Collectors.toList());
            int size = listWithoutDuplicates.size();
            for (int i = 0; i < size; i++){
                listWithoutDuplicates.add(listWithoutDuplicates.get(i) + 1);
            }
            
          //  System.out.println(listWithoutDuplicates);
            fiscalYearText.setModel(new DefaultComboBoxModel(listWithoutDuplicates.toArray()));

            
        }
        catch (Exception x){
            x.printStackTrace();
        }
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        ytdScoreText = new javax.swing.JTextField();
        fiscalYearText = new javax.swing.JComboBox<>();
        refreshButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        quarter1Text = new javax.swing.JTextField();
        quarter2Text = new javax.swing.JTextField();
        quarter3Text = new javax.swing.JTextField();
        quarter4Text = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Average scores per Fiscal Year");

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel1.setText("YTD Average");

        jLabel2.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel2.setText("Fiscal year");
        jLabel2.setToolTipText("");

        refreshButton.setText("Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel3.setText("Quarter 1");

        jLabel4.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel4.setText("Quarter 2");

        jLabel5.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel5.setText("Quarter 3");

        jLabel6.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        jLabel6.setText("Quarter 4");

        jLabel7.setFont(new java.awt.Font("sansserif", 1, 18)); // NOI18N
        jLabel7.setText("Average Score per Fiscal Year");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel4)
                                .addComponent(jLabel5)
                                .addComponent(jLabel6))
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(quarter3Text)
                            .addComponent(quarter2Text)
                            .addComponent(quarter1Text)
                            .addComponent(ytdScoreText)
                            .addComponent(refreshButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(quarter4Text)
                            .addComponent(fiscalYearText, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel7)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(fiscalYearText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(refreshButton)
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ytdScoreText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(quarter1Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(quarter2Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(quarter3Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(quarter4Text, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        // TODO add your handling code here:
        SQLConnect conn = new SQLConnect();
        int yearSelected = Integer.valueOf((Integer)fiscalYearText.getSelectedItem());
        int yearBegin = yearSelected - 1;
        String year = fiscalYearText.getSelectedItem().toString();
        String scoreQuery = "SELECT Create_Date, JTScore FROM tblTraining_HDR";
        String quarter1Query = "SELECT Create_Date, JTScore FROM tblTraining_HDR WHERE Create_Date > '" + yearBegin + "-03-01' AND Create_Date"
                + " < '" + yearBegin + "-06-01'";       
        String quarter2Query = "SELECT Create_Date, JTScore FROM tblTraining_HDR WHERE Create_Date > '" + yearBegin + "-06-02' AND Create_Date"
                + " < '" + yearBegin + "-09-01'";
        String quarter3Query = "SELECT Create_Date, JTScore FROM tblTraining_HDR WHERE Create_Date > '" + yearBegin + "-09-02' AND Create_Date"
                + " < '" + yearBegin + "-12-01'";
        String quarter4Query = "SELECT Create_Date, JTScore FROM tblTraining_HDR WHERE Create_Date > '" + yearBegin + "-12-02' AND Create_Date"
                + " < '" + yearSelected + "-03-01'";
        
        String score = null;
        ArrayList<Integer> scoreNum = new ArrayList<Integer>();
        Date date = new Date();
        try{
            con = conn.SQLConnect();
            stmt = con.createStatement();
            rs = stmt.executeQuery(scoreQuery);
            
            while (rs.next()){
                score = rs.getString("JTScore");
                switch(score){
                    case "Needs Training":
                        scoreNum.add(1);
                        break;
                    case "Adequate":
                        scoreNum.add(2);
                        break;
                    case "Proficient":
                        scoreNum.add(3);
                        break;
                }
                
                
            }
            Double avg = scoreNum.stream().mapToInt(val -> val).average().orElse(0.0);
            Double roundedytd = round(avg, 2);
            ytdScoreText.setText(roundedytd.toString());
            rs.close();
            scoreNum.clear();
            
            rs = stmt.executeQuery(quarter1Query);
            
            while (rs.next()){
                score = rs.getString("JTScore");
                switch(score){
                    case "Needs Training":
                        scoreNum.add(1);
                        break;
                    case "Adequate":
                        scoreNum.add(2);
                        break;
                    case "Proficient":
                        scoreNum.add(3);
                        break;
                }
            }
            Double q1Avg = scoreNum.stream().mapToInt(val -> val).average().orElse(0.0);
            Double rounded = round(q1Avg, 2);
            quarter1Text.setText(rounded.toString());
            rs.close();
            scoreNum.clear();
            
            rs = stmt.executeQuery(quarter2Query);
            
            while (rs.next()){
                score = rs.getString("JTScore");
                switch(score){
                    case "Needs Training":
                        scoreNum.add(1);
                        break;
                    case "Adequate":
                        scoreNum.add(2);
                        break;
                    case "Proficient":
                        scoreNum.add(3);
                        break;
                }
            }
            Double q2Avg = scoreNum.stream().mapToInt(val -> val).average().orElse(0.0);
            Double rounded2 = round(q2Avg, 2);
            quarter2Text.setText(rounded2.toString());
            rs.close();
            scoreNum.clear();
            
            rs = stmt.executeQuery(quarter3Query);
            
            while (rs.next()){
                score = rs.getString("JTScore");
                switch(score){
                    case "Needs Training":
                        scoreNum.add(1);
                        break;
                    case "Adequate":
                        scoreNum.add(2);
                        break;
                    case "Proficient":
                        scoreNum.add(3);
                        break;
                }
            }
            Double q3Avg = scoreNum.stream().mapToInt(val -> val).average().orElse(0.0);
            Double rounded3 = round(q3Avg, 2);
            quarter3Text.setText(rounded3.toString());
            rs.close();
            scoreNum.clear();
            
            rs = stmt.executeQuery(quarter4Query);
            
            while (rs.next()){
                score = rs.getString("JTScore");
                switch(score){
                    case "Needs Training":
                        scoreNum.add(1);
                        break;
                    case "Adequate":
                        scoreNum.add(2);
                        break;
                    case "Proficient":
                        scoreNum.add(3);
                        break;
                }
            }
            Double q4Avg = scoreNum.stream().mapToInt(val -> val).average().orElse(0.0);
            Double rounded4 = round(q4Avg, 2);
            quarter4Text.setText(rounded4.toString());
            rs.close();
            scoreNum.clear();
            con.close();
            stmt.close();
        }
        catch (Exception x){
            x.printStackTrace();
            JOptionPane.showMessageDialog(null, x);
        }
        
    }//GEN-LAST:event_refreshButtonActionPerformed
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ScoringViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ScoringViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ScoringViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ScoringViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ScoringViewer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> fiscalYearText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField quarter1Text;
    private javax.swing.JTextField quarter2Text;
    private javax.swing.JTextField quarter3Text;
    private javax.swing.JTextField quarter4Text;
    private javax.swing.JButton refreshButton;
    private javax.swing.JTextField ytdScoreText;
    // End of variables declaration//GEN-END:variables
}
